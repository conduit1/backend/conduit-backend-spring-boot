package io.realworld.conduit.models;

import javax.persistence.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToMany
    @JoinTable(name = "user_followed_user", inverseJoinColumns = @JoinColumn(name = "followed_user_id"))
    private List<User> followedUsers;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String hashed_password;

    private String bio;
    private URL image;

    public User(String email, String username, String hashed_password, String bio, URL image) {
        this.email = email;
        this.username = username;
        this.hashed_password = hashed_password;
        this.bio = bio;
        this.image = image;
        this.followedUsers = new ArrayList<>();
    }

    public User() {}

    public List<User> getFollowedUsers() {
        return followedUsers;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getHashed_password() {
        return hashed_password;
    }

    public String getBio() {
        return bio;
    }

    public URL getImage() {
        return image;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setHashed_password(String hashed_password) {
        this.hashed_password = hashed_password;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public void setImage(URL image) {
        this.image = image;
    }

    public void addFollowedUser(User followedUser) {
        this.followedUsers.add(followedUser);
    }
}
