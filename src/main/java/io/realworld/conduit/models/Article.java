package io.realworld.conduit.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User author;

    @ManyToMany
    @JoinTable(name = "article_tag", inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;

    @ManyToMany
    @JoinTable(name = "article_favorer", inverseJoinColumns = @JoinColumn(name = "favorer_id"))
    private List<User> favorers;

    @Column(nullable = false)
    private String title;

    @Column(unique = true, nullable = false)
    private String slug;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String body;

    @Column(nullable = false)
    private Date createdAt;

    private Date updatedAt;

    public Article(User author, List<Tag> tags, String title, String slug, String description, String body, Date createdAt) {
        this.author = author;
        this.tags = tags;
        this.title = title;
        this.slug = slug;
        this.description = description;
        this.body = body;
        this.createdAt = createdAt;
        this.favorers = new ArrayList<>();
    }

    public Article() {}

    public User getAuthor() {
        return author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public List<User> getFavorers() {
        return favorers;
    }

    public String getTitle() {
        return title;
    }

    public String getSlug() {
        return slug;
    }

    public String getDescription() {
        return description;
    }

    public String getBody() {
        return body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void addFavorer(User favorer) {
        this.favorers.add(favorer);
    }
}
