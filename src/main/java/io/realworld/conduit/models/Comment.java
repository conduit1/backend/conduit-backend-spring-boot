package io.realworld.conduit.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User author;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Article commentedArticle;

    @Column(nullable = false)
    private String body;

    @Column(nullable = false)
    private Date createdAt;

    private Date updatedAt;

    public Comment(User author, Article commentedArticle, String body, Date createdAt) {
        this.author = author;
        this.commentedArticle = commentedArticle;
        this.body = body;
        this.createdAt = createdAt;
    }

    public Comment() {}

    public long getId() {
        return id;
    }

    public User getAuthor() {
        return author;
    }

    public Article getCommentedArticle() {
        return commentedArticle;
    }

    public String getBody() {
        return body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
